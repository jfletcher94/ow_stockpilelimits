using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using HarmonyLib;
using TenCrowns.GameCore;
using TenCrowns.GameCore.Text;

namespace Stockpile_Limits {
  [HarmonyPatch(typeof(HelpText))]
  public class PatchHelpText {
    private static readonly MethodInfo PlayerGetMaxOrders = AccessTools.Method(typeof(Player), "getMaxOrders");

    [HarmonyTranspiler]
    [HarmonyPatch(typeof(HelpText), "buildEffectPlayerHelp")]
    static IEnumerable<CodeInstruction> TranspilerBuildEffectPlayerHelp(
      IEnumerable<CodeInstruction> instructions, ILGenerator ilGenerator
    ) {
      MethodInfo infos = AccessTools.Method(typeof(HelpText), "infos");
      MethodInfo effectPlayer = AccessTools.Method(typeof(Infos), "effectPlayer");
      MethodInfo getAdditionalData = AccessTools.Method(typeof(InfoEffectPlayerExtension), "GetAdditionalData");
      MethodInfo textvar = AccessTools.Method(typeof(TextExtensions), "TEXTVAR", new Type[] { typeof(int) });
      MethodInfo buildTurnScaleNamePlural = AccessTools.Method(typeof(HelpText), "buildTurnScaleNamePlural");
      MethodInfo addText = AccessTools.Method(
        typeof(TextBuilder), "AddTEXT",
        new Type[] {
          typeof(string), typeof(TextVariable), typeof(TextVariable), typeof(TextVariable), typeof(TextVariable),
          typeof(bool)
        });
      FieldInfo miMaxTraining = AccessTools.Field(typeof(InfoEffectPlayerAdditionalData), "miMaxTraining");
      FieldInfo miMaxCivics = AccessTools.Field(typeof(InfoEffectPlayerAdditionalData), "miMaxCivics");
      Label transpilerDone = ilGenerator.DefineLabel();
      Label nextSection = ilGenerator.DefineLabel();

      State state = State.Ready;
      foreach (var instruction in instructions) {
        switch (state) {
          case State.Finished:
            yield return instruction;
            break;
          case State.Ready:
            // builder.AddTEXT("TEXT_HELPTEXT_EFFECT_PLAYER_HELP_MAX_ACTIONS", ...);
            if (instruction.opcode == OpCodes.Ldstr
              && "TEXT_HELPTEXT_EFFECT_PLAYER_HELP_MAX_ACTIONS".Equals((string) instruction.operand)) {
              state = State.WithinPrecedingBlock;
            }
            yield return instruction;
            break;
          case State.WithinPrecedingBlock:
            if (instruction.opcode == OpCodes.Nop) {
              state = State.EndOfPrecedingBlock;
            }
            yield return instruction;
            break;
          case State.EndOfPrecedingBlock:
            // Preceding code block can be arbitrarily deeply nested within {}
            if (instruction.opcode == OpCodes.Nop) {
              yield return instruction;
              break;
            }
            // int iValue0 = infos().effectPlayer(eEffectPlayer).GetAdditionalData().miMaxTraining;
            LocalBuilder iValue0 = ilGenerator.DeclareLocal(typeof(int));
            yield return new CodeInstruction(OpCodes.Ldarg_0);
            yield return new CodeInstruction(OpCodes.Call, infos);
            yield return new CodeInstruction(OpCodes.Ldarg_2);
            yield return new CodeInstruction(OpCodes.Callvirt, effectPlayer);
            yield return new CodeInstruction(OpCodes.Call, getAdditionalData);
            yield return new CodeInstruction(OpCodes.Ldfld, miMaxTraining);
            yield return new CodeInstruction(OpCodes.Stloc_S, iValue0);
            // if (iValue0 != 0)
            yield return new CodeInstruction(OpCodes.Ldloc_S, iValue0);
            yield return new CodeInstruction(OpCodes.Brfalse_S, nextSection);
            // builder.AddTEXT(
            //   "TEXT_HELPTEXT_EFFECT_PLAYER_HELP_MAX_TRAINING", TEXTVAR(iValue0), buildTurnScaleNamePlural(pGame));
            yield return new CodeInstruction(OpCodes.Ldarg_1);
            yield return new CodeInstruction(OpCodes.Ldstr, "TEXT_HELPTEXT_EFFECT_PLAYER_HELP_MAX_TRAINING");
            yield return new CodeInstruction(OpCodes.Ldloc_S, iValue0);
            yield return new CodeInstruction(OpCodes.Call, textvar);
            yield return new CodeInstruction(OpCodes.Ldarg_0);
            yield return new CodeInstruction(OpCodes.Ldarg_3);
            yield return new CodeInstruction(OpCodes.Callvirt, buildTurnScaleNamePlural);
            yield return new CodeInstruction(OpCodes.Ldnull);
            yield return new CodeInstruction(OpCodes.Ldnull);
            yield return new CodeInstruction(OpCodes.Ldc_I4_0);
            yield return new CodeInstruction(OpCodes.Callvirt, addText);
            yield return new CodeInstruction(OpCodes.Pop);

            // int iValue1 = infos().effectPlayer(eEffectPlayer).GetAdditionalData().miMaxCivics;
            LocalBuilder iValue1 = ilGenerator.DeclareLocal(typeof(int));
            CodeInstruction startNextSection = new CodeInstruction(OpCodes.Ldarg_0);
            startNextSection.labels.Add(nextSection);
            yield return startNextSection;
            yield return new CodeInstruction(OpCodes.Call, infos);
            yield return new CodeInstruction(OpCodes.Ldarg_2);
            yield return new CodeInstruction(OpCodes.Callvirt, effectPlayer);
            yield return new CodeInstruction(OpCodes.Call, getAdditionalData);
            yield return new CodeInstruction(OpCodes.Ldfld, miMaxCivics);
            yield return new CodeInstruction(OpCodes.Stloc_S, iValue1);
            // if (iValue1 != 0)
            yield return new CodeInstruction(OpCodes.Ldloc_S, iValue1);
            yield return new CodeInstruction(OpCodes.Brfalse_S, transpilerDone);
            // builder.AddTEXT(
            //   "TEXT_HELPTEXT_EFFECT_PLAYER_HELP_MAX_CIVICS", TEXTVAR(iValue1), buildTurnScaleNamePlural(pGame));
            yield return new CodeInstruction(OpCodes.Ldarg_1);
            yield return new CodeInstruction(OpCodes.Ldstr, "TEXT_HELPTEXT_EFFECT_PLAYER_HELP_MAX_CIVICS");
            yield return new CodeInstruction(OpCodes.Ldloc_S, iValue1);
            yield return new CodeInstruction(OpCodes.Call, textvar);
            yield return new CodeInstruction(OpCodes.Ldarg_0);
            yield return new CodeInstruction(OpCodes.Ldarg_3);
            yield return new CodeInstruction(OpCodes.Callvirt, buildTurnScaleNamePlural);
            yield return new CodeInstruction(OpCodes.Ldnull);
            yield return new CodeInstruction(OpCodes.Ldnull);
            yield return new CodeInstruction(OpCodes.Ldc_I4_0);
            yield return new CodeInstruction(OpCodes.Callvirt, addText);
            yield return new CodeInstruction(OpCodes.Pop);

            instruction.labels.Add(transpilerDone);
            yield return instruction;
            state = State.Finished;
            break;
        }
      }
    }

    [HarmonyTranspiler]
    [HarmonyPatch(typeof(HelpText), "buildYieldNetHelp")]
    static IEnumerable<CodeInstruction> TranspilerbuildYieldNetHelp(
      IEnumerable<CodeInstruction> instructions, ILGenerator ilGenerator
    ) {
      MethodInfo helper = AccessTools.Method(typeof(PatchHelpText), "buildYieldNetHelpHelper");

      State state = State.Ready;
      foreach (var instruction in instructions) {
        switch (state) {
          case State.Finished:
            yield return instruction;
            break;
          case State.Ready:
            // builder.AddTEXT("TEXT_HELPTEXT_YIELD_NET_CARAVAN_MISSIONS", ...);
            if (instruction.opcode == OpCodes.Ldstr
              && "TEXT_HELPTEXT_YIELD_NET_CARAVAN_MISSIONS".Equals((string) instruction.operand)) {
              state = State.WithinPrecedingBlock;
            }
            yield return instruction;
            break;
          case State.WithinPrecedingBlock:
            if (instruction.opcode == OpCodes.Pop) {
              state = State.EndOfPrecedingBlock;
            }
            yield return instruction;
            break;
          case State.EndOfPrecedingBlock:
            // Preceding code block can be arbitrarily deeply nested within {}
            if (instruction.opcode == OpCodes.Nop) {
              yield return instruction;
              break;
            }
            // PatchHelpText.buildYieldNetHelpHelper(this, builder, eYield, pPlayer);
            CodeInstruction start = new CodeInstruction(OpCodes.Ldarg_0);
            start.labels.AddRange(instruction.labels);
            yield return start;
            yield return new CodeInstruction(OpCodes.Ldarg_1);
            yield return new CodeInstruction(OpCodes.Ldarg_2);
            yield return new CodeInstruction(OpCodes.Ldarg_S, 4);
            yield return new CodeInstruction(OpCodes.Call, helper);
            yield return new CodeInstruction(OpCodes.Nop);

            instruction.labels.Clear();
            yield return instruction;
            state = State.Finished;
            break;
        }
      }
    }

    public static void buildYieldNetHelpHelper(
      HelpText helpText, TextBuilder builder, YieldType eYield, Player pPlayer
    ) {
      if (eYield == StockpileLimits.ModSettings.Infos.Globals.ORDERS_YIELD) {
        buildYieldNetHelpHelperHelper(
          helpText, builder, pPlayer, 0, p => p.miMaxOrders, p => (int) PlayerGetMaxOrders.Invoke(p, null));
      } else if (eYield == StockpileLimits.ModSettings.Infos.Globals.TRAINING_YIELD) {
        buildYieldNetHelpHelperHelper(
          helpText, builder, pPlayer, StockpileLimits.ModSettings.Infos.Helpers.getMaxTraining(),
          p => p.GetAdditionalData().miMaxTraining, p => StockpileLimits.ModSettings.Infos.Helpers.getMaxTraining(p),
          Constants.YIELDS_MULTIPLIER);
      } else if (eYield == StockpileLimits.ModSettings.Infos.Globals.CIVICS_YIELD) {
        buildYieldNetHelpHelperHelper(
          helpText, builder, pPlayer, StockpileLimits.ModSettings.Infos.Helpers.getMaxCivics(),
          p => p.GetAdditionalData().miMaxCivics, p => StockpileLimits.ModSettings.Infos.Helpers.getMaxCivics(p),
          Constants.YIELDS_MULTIPLIER);
      }
    }

    private static void buildYieldNetHelpHelperHelper(
      HelpText helpText, TextBuilder builder, Player pPlayer, int iBaseMaxValue,
      Func<InfoEffectPlayer, int> effectPlayerMaxValueGetter, Func<Player, int> playerMaxValueGetter,
      int iMultiplier = 0
    ) {
      using (TextBuilder subBuilder = TextBuilder.GetTextBuilder(helpText.TextManager)) {
        using (subBuilder.BeginScope(TextBuilder.ScopeType.BULLET)) {
          if (iBaseMaxValue > 0) {
            subBuilder.Add(
              helpText.buildColonSpaceOne(
                helpText.buildYieldTextVariable(iBaseMaxValue, iMultiplier: iMultiplier),
                helpText.TEXTVAR_TYPE("TEXT_HELPTEXT_STOCKPILE_MAXIMUM_BASE_HELPER")));
          }
          for (EffectPlayerType eLoopEffectPlayer = 0;
            eLoopEffectPlayer < StockpileLimits.ModSettings.Infos.effectPlayersNum();
            eLoopEffectPlayer++) {
            int iCount = pPlayer.getEffectPlayerCount(eLoopEffectPlayer);
            if (iCount <= 0) {
              continue;
            }
            int iMaxValue = effectPlayerMaxValueGetter.Invoke(
              StockpileLimits.ModSettings.Infos.effectPlayer(eLoopEffectPlayer));
            if (iMaxValue <= 0) {
              continue;
            }
            subBuilder.Add(
              helpText.buildColonSpaceTwo(
                helpText.buildYieldTextVariable(iCount * iMaxValue),
                helpText.buildEffectPlayerLinkVariable(eLoopEffectPlayer),
                iCount > 1
                  ? helpText.TEXTVAR_TYPE("TEXT_HELPTEXT_PARENTHESIS_MULTIPLIER", iCount)
                  : TextExtensions.TEXTVAR("")));
          }
        }
        if (subBuilder.HasContent) {
          helpText.buildDividerText(builder);
          builder.AddTEXT(
            "TEXT_HELPTEXT_STOCKPILE_MAXIMUM_TOTAL_HELPER",
            helpText.buildYieldTextVariable(
              playerMaxValueGetter.Invoke(pPlayer), iMultiplier: iMultiplier));
          builder.Add(subBuilder.ToTextVariable());
        }
      }
    }

    private enum State {
      Ready,
      WithinPrecedingBlock,
      EndOfPrecedingBlock,
      Finished
    }
  }
}