using System;
using TenCrowns.GameCore;

namespace Stockpile_Limits {
  public static class InfoHelpersExtension {
    public static int getMaxTraining(this InfoHelpers infoHelpers, Player pPlayer = null) {
      return infoHelpers.getMaxTraining() + getMaxStockpileHelper(pPlayer, data => data.miMaxTraining);
    }

    public static int getMaxCivics(this InfoHelpers infoHelpers, Player pPlayer = null) {
      return infoHelpers.getMaxCivics() + getMaxStockpileHelper(pPlayer, data => data.miMaxCivics);
    }

    private static int getMaxStockpileHelper(Player pPlayer, Func<InfoEffectPlayerAdditionalData, int> maxValueGetter) {
      if (StockpileLimits.ModSettings.Infos == null || pPlayer == null) {
        return 0;
      }

      int iExtra = 0;
      for (EffectPlayerType eLoopEffectPlayer = 0;
        eLoopEffectPlayer < StockpileLimits.ModSettings.Infos.effectPlayersNum();
        eLoopEffectPlayer++) {
        int iCount = pPlayer.getEffectPlayerCount(eLoopEffectPlayer);
        if (iCount <= 0) {
          continue;
        }
        int iMaxValue = maxValueGetter.Invoke(
          StockpileLimits.ModSettings.Infos.effectPlayer(eLoopEffectPlayer).GetAdditionalData());
        if (iMaxValue <= 0) {
          continue;
        }
        iExtra += iCount * iMaxValue * Constants.YIELDS_MULTIPLIER;
      }
      return iExtra;
    }
  }
}