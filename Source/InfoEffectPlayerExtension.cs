using System.Runtime.CompilerServices;
using TenCrowns.GameCore;

namespace Stockpile_Limits {
  public static class InfoEffectPlayerExtension {
    private static readonly ConditionalWeakTable<InfoEffectPlayer, InfoEffectPlayerAdditionalData> data =
      new ConditionalWeakTable<InfoEffectPlayer, InfoEffectPlayerAdditionalData>();

    public static InfoEffectPlayerAdditionalData GetAdditionalData(this InfoEffectPlayer infoEffectPlayer) {
      return data.GetOrCreateValue(infoEffectPlayer);
    }
  }
}