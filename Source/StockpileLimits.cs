﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using HarmonyLib;
using TenCrowns.AppCore;
using TenCrowns.GameCore;

namespace Stockpile_Limits
{
    public class StockpileLimits : ModEntryPointAdapter
    {
        public const string MY_HARMONY_ID = "Thremtopod.StockpileLimits.patch";
        public static Harmony harmony;
        public static ModSettings ModSettings;

        public override void Initialize(ModSettings modSettings)
        {
            if (harmony != null)
            {
                return;
            }

            CustomXmlTags.CustomXmlTags.AppendTags<InfoEffectPlayer, InfoEffectPlayerAdditionalData>(
                player => player.GetAdditionalData());
            harmony = new Harmony(MY_HARMONY_ID);
            harmony.PatchAll();
            ModSettings = modSettings;
        }

        public override void Shutdown()
        {
            if (harmony == null)
            {
                return;
            }

            harmony.UnpatchAll(MY_HARMONY_ID);
            harmony = null;
            ModSettings = null;
        }
    }

    [HarmonyPatch(typeof(Player))]
    public class PatchPlayer
    {
        [HarmonyTranspiler]
        [HarmonyPatch(typeof(Player), "processTurn")]
        static IEnumerable<CodeInstruction> TranspilerProcessTurn(
            IEnumerable<CodeInstruction> instructions, ILGenerator ilGenerator
        )
        {
            return PatchHelper.transpiler(instructions, true, true,
                new KeyValuePair<OpCode, object>(OpCodes.Ldarg_0, null));
        }
    }

    [HarmonyPatch(typeof(Player.PlayerAI))]
    public class PatchPlayerAI
    {
        [HarmonyTranspiler]
        [HarmonyPatch(typeof(Player.PlayerAI), "calculateYieldValue")]
        static IEnumerable<CodeInstruction> TranspilerCalculateYieldValue(
            IEnumerable<CodeInstruction> instructions, ILGenerator ilGenerator
        )
        {
            FieldInfo player = AccessTools.Field(typeof(Player.PlayerAI), "player");
            return PatchHelper.transpiler(
                instructions, true, true, new KeyValuePair<OpCode, object>(OpCodes.Ldarg_0, null),
                new KeyValuePair<OpCode, object>(OpCodes.Ldfld, player));
        }
    }

    [HarmonyPatch(typeof(InfoHelpers))]
    public class PatchInfoHelpers
    {
        [HarmonyTranspiler]
        [HarmonyPatch(typeof(InfoHelpers), "getStartLawCost")]
        static IEnumerable<CodeInstruction> TranspilerGetStartLawCost(
            IEnumerable<CodeInstruction> instructions, ILGenerator ilGenerator
        )
        {
            return PatchHelper.transpiler(instructions, false, true,
                new KeyValuePair<OpCode, object>(OpCodes.Ldarg_2, null));
        }

        [HarmonyTranspiler]
        [HarmonyPatch(typeof(InfoHelpers), "getAdoptReligionCost")]
        static IEnumerable<CodeInstruction> TranspilerGetAdoptReligionCost(
            IEnumerable<CodeInstruction> instructions, ILGenerator ilGenerator
        )
        {
            return PatchHelper.transpiler(instructions, false, true,
                new KeyValuePair<OpCode, object>(OpCodes.Ldarg_2, null));
        }
    }

    public static class PatchHelper
    {
        private static readonly MethodInfo GetMaxTraining = AccessTools.Method(typeof(InfoHelpers), "getMaxTraining");

        private static readonly MethodInfo ExtensionGetMaxTraining = AccessTools.Method(
            typeof(InfoHelpersExtension), "getMaxTraining", new Type[] { typeof(InfoHelpers), typeof(Player) });

        private static readonly MethodInfo GetMaxCivics = AccessTools.Method(typeof(InfoHelpers), "getMaxCivics");

        private static readonly MethodInfo ExtensionGetMaxCivics = AccessTools.Method(
            typeof(InfoHelpersExtension), "getMaxCivics", new Type[] { typeof(InfoHelpers), typeof(Player) });

        public static IEnumerable<CodeInstruction> transpiler(
            IEnumerable<CodeInstruction> instructions, bool training, bool civics,
            params KeyValuePair<OpCode, object>[] codes
        )
        {
            foreach (var instruction in instructions)
            {
                if (instruction.opcode == OpCodes.Callvirt || instruction.opcode == OpCodes.Call)
                {
                    if (training && instruction.OperandIs(GetMaxTraining))
                    {
                        // replace `InfoHelpers.getMaxTraining()` with `InfoHelpers.getMaxTraining(Player)`
                        foreach (KeyValuePair<OpCode, object> code in codes)
                        {
                            yield return new CodeInstruction(code.Key, code.Value);
                        }

                        yield return new CodeInstruction(OpCodes.Call, ExtensionGetMaxTraining);
                        continue;
                    }

                    if (civics && instruction.OperandIs(GetMaxCivics))
                    {
                        // replace `InfoHelpers.getMaxCivics()` with `InfoHelpers.getMaxCivics(Player)`
                        foreach (KeyValuePair<OpCode, object> code in codes)
                        {
                            yield return new CodeInstruction(code.Key, code.Value);
                        }

                        yield return new CodeInstruction(OpCodes.Call, ExtensionGetMaxCivics);
                        continue;
                    }
                }

                yield return instruction;
            }
        }
    }
}