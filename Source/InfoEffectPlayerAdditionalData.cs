using System;
using CustomXmlTags;
using TenCrowns.GameCore;

namespace Stockpile_Limits {
  [Serializable]
  public class InfoEffectPlayerAdditionalData : IInfoAdditionalData<InfoEffectPlayer> {
    [XmlTag("iMaxCivics")] public int miMaxCivics = 0;
    [XmlTag("iMaxTraining")] public int miMaxTraining = 0;
  }
}